﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowScript : MonoBehaviour {

    bool _shadow = false;
    public SpriteRenderer _sprite;

    void OnMouseDown()
    {
        if (_shadow)
        {
            _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 0f);
            _shadow = !_shadow;
        }
        else
        {
            _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 0.4f);
            _shadow = !_shadow;
        }
    }
   
}
