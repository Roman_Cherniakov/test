﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {

    public SpriteRenderer _sprite;
    RaycastHit2D _rch;
    public float _point1X;
    public float _point1Y;
    public float _point2X = -4.18f;
    public float _point2Y = 1.69f;
    public float _point3X = -1.64f;
    public float _point3Y = 3.05f;
    public float _point4X = 3.78f;
    public float _point4Y = -2.14f;
    Vector2 _pos1;
    Vector2 _pos2;
    Vector2 _pos3;
    Vector2 _pos4;
    bool _flagpoint1 = true;
    bool _flagpoint2= false;
    bool _flagpoint3 = false;
    bool _flagpoint4 = false;
    bool boolean = true;

	void Start () {
        _point1X = this.transform.position.x;
        _point1Y = this.transform.position.y;
        _pos1 = new Vector2(_point1X, _point1Y);
        _pos2 = new Vector2(_point2X, _point2Y);
        _pos3 = new Vector2(_point3X, _point3Y);
        _pos4 = new Vector2(_point4X, _point4Y);
	}
	
	void Update () {        
        if (_flagpoint1 == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, _pos2, Time.deltaTime * 2);
            if (Vector3.Distance(transform.position, _pos2) < 0.001)
            {
                _flagpoint1 = !_flagpoint1;
                _flagpoint2 = !_flagpoint2;
            }            
        }
        if (_flagpoint2 == true)
        {
            this.transform.localScale = new Vector2(0.3f, 0.3f);
            transform.position = Vector3.MoveTowards(transform.position, _pos3, Time.deltaTime * 2);
            if (Vector3.Distance(transform.position, _pos3) < 0.001)
            {
                _flagpoint2 = !_flagpoint2;
                _flagpoint3 = !_flagpoint3;
            }
        }
        if (_flagpoint3 == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, _pos4, Time.deltaTime * 2);
            if (Vector3.Distance(transform.position, _pos4) < 0.001)
            {
                _flagpoint3 = !_flagpoint3;
                _flagpoint4 = !_flagpoint4;
            }
        }
        if (_flagpoint4 == true)
        {
            this.transform.localScale = new Vector2(-0.3f, 0.3f);
            transform.position = Vector3.MoveTowards(transform.position, _pos1, Time.deltaTime * 2);
            if (Vector3.Distance(transform.position, _pos1) < 0.001)
            {
                _flagpoint4 = !_flagpoint4;
                _flagpoint1 = !_flagpoint1;
            }
        }
	}
}